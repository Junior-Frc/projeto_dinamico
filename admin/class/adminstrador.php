<?php
 // = atribuição
        // == comparação
        // === absoluto igual (tipo e valor)
        // => associação (FETCH_ASSOC)
        // -> atributo, método(s), objeto
class administrador{
    private $id;
    private $nome;
    private $email;
    private $senha;

    public function getId(){
        return $this->id;
    }
    public function setId($value){
        $this->id=$value;
    }
    public function getNome(){
        return $this->nome;
    }
    public function setNome($value){
        $this->nome=$value;
    }
    public function getEmail(){
        return $this->email;
    }
    public function setemail($value){
        $this->email=$value;
    }
    public function getSenha(){
        return $this->senha;
    }
    public function setsenha($value){
        $this->senha=$value;
    }

    public static function loadById($id){
        $sql = new sql();
        $results = $sql->select("SELECT * FROM administrador WHERE id = :id",array(":id"=>$id));
        if (count($results)>0){
            $this->SetData($results[0]);
        } 
    }
    public static function getList(){
        $sql = new Sql();
        return $sql->select("SELECT * FROM administrador ORDER BY nome");
    }
    public static function search($adm){
        $sql = new sql();
        return $sql->select("SELECT * FROM administrador WHERE nome LIKE :nome", array(":nome"=>"%".$adm."%"))
    }
    public function login($email, $senha){
        $senhacript = md5($senha);
        $sql = new sql();
        $results = $sql->select("SELECT * FROM adminstrador WHERE email AND senha = :senha", array(":email"=>$email, ":senha"=>senhacript));
        if(count($results)>){
            $this->setData($results[0]);
        }
    }
    public function setData($data){
        $this->setId($data['id']);
        $this->setId($data['nome']);
        $this->setId($data['email']);
        $this->setId($data['senha']);
    }
    public function insert(){
        $sql = new Sql();
        $results = $sql->select("CALL sp_adm_insert(:nome, :email, :senha)", array(
            ":nome"=>$this->getNome(),
            ":email"=>$this->getEmail(),
            ":senha"=>$this->getSenha(),
        ));
        if(count($results)>0){
            //$this->setData($results[0]);
            return $results[0];
        }
    }
    public function update($id, $nome, $senha){
        $this->setNome($nome);
        $this->setSenha($senha);
        $sql = new Sql();
        $sql->query("UPDATE administrador SET nome = :nome, senha = :senha WHERE id = :id", array(
            ":id"=>$id,
            ":nome"=>$nome,
            ":senha"=> md5($senha)
        ));
    }
    public function delete(){
        $sql = new Sql();
        $sql->query("DELETE FROM administrador WHERE id = :id", array(":id"=>$this->getId()));
    }
    // Criando métodos construtores no PHP
    public function __construct($nome="",$email="",$senha=""){
        $this->nome=$nome;
        $this->email=$email;
        $this->$senha = $senha
    }
    public function __tostring(){
        return json_encode(array(
            ":id"=>$this->getId,            
            ":nome"=>$this->getNome(),
            ":email"=>$this->getEmail(),
            ":senha"=>$this->getSenha(),
        ));
    }
}
?>



